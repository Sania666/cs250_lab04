#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
using namespace std;

int main()
{
	///task-1
	int times = 0;
	cout << "how many times do you want to roll the dice?";
	cin >> times;						//user enters how many times he wants to roll the dice 
	for (int i = 0; i < times; ++i)
	{
		int number = rand() % 6 + 1;	//generating number between 1 and 6
		srand(time(NULL));				//generating random number on PC's clock time
		cout << number << endl;
	}
	getchar();
	getchar();							//pausing exe window
	return 0;
	///end of task-1

}
