#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include<vector>
#include <algorithm>
using namespace std;
// declarations for the functions implemented
bool MovMin(vector <int>&t1, vector<int> &t2);
void TestmovMin();
bool comp(const int& num1, const int& num2);
int main()
{
	TestmovMin();
	getchar();
	getchar();
	return 0;
}
bool MovMin(vector <int>&t1, vector<int> &t2) // sorting with nested loop
{
	int temp;
	for (int i = 1; i < t1.size(); i++)
	{
		for (int n = 0; n <i; n++)
		{
			if (t1[n] > t1[i])
			{
				temp = t1[n];		//swaping minimum value
				t1[n] = t1[i];
				t1[i] = temp;
			}
		}
	}
	if (t1 == t2){
		return true;
	}
	else{
		return false;
	}

}
void TestmovMin()		// test function
{

	time_t seconds;
	time(&seconds);
	srand((unsigned int)seconds);
	int value;
	int max = 0;			//maximum value of the vector
	int i = rand() % 100 + 1;
	vector<int> t1;
	for (int j = 0; j < i; j++)
	{
		value = rand() % 100 + 1;	// generating a random vector
		t1.push_back(value);
		if (value>max)
		{
			max = value;
		}
	}
	max = max - 2;
	int lastindex = rand() % max + 1;
	//sorting first vector
	sort(t1.begin(), t1.end(), comp);
	t1.push_back(lastindex);
	vector<int> t2 = t1;
	//sorting second vector
	sort(t2.begin(), t2.end(), comp);
	if (MovMin(t1, t2))
	{
		cout << "you're good" << endl;
	}
	else
	{
		cout << "'not so good" << endl;
	}

}
bool comp(const int& num1, const int& num2)		//function to tell to sort in ascending order
{
	return num1 < num2;
}
